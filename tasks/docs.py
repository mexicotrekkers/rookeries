"""
Tasks for building documentation.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import invoke as inv

from . import utils


@inv.task
def build():
    """Creates the HTML documentation through Sphinx."""
    inv.run('sphinx-apidoc -o docs/_api rookeries', echo=True)
    inv.run('sphinx-build -b html docs docs/_build', echo=True)


@inv.task
def clean(dry_run=False):
    """
    Clean up the generated Sphinx documentation.

    :param dry_run: Mode for switching between a dry run and actual deletion run.
    """
    sphinx_generated_folders = ['docs/_api', 'docs/_build']
    utils.delete_in_path_list(sphinx_generated_folders, dry_run=dry_run)
