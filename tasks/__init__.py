"""
Build tasks for Rookeries.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import invoke as inv

from . import clean, deploy, docs, server, test, vagrant


ns = inv.Collection()
ns.add_collection(clean)
ns.add_collection(deploy)
ns.add_collection(docs)
ns.add_collection(server)
ns.add_collection(test)
ns.add_collection(vagrant)
