"""
Tasks for cleaning compiled resources.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import fnmatch
import os

import invoke as inv

from . import docs, test


@inv.task
def python():
    """Cleans out all compiled Python bytecode and object files."""
    for root, dirs, files in os.walk(os.curdir):
        for _file in files:
            if fnmatch.fnmatch(_file, '*.pyc'):
                os.remove(os.path.join(root, _file))


@inv.task
def assets():
    """Cleans up after web asset compilation."""
    LESS_PATH = 'rookeries/static/css'
    SCRIPTS_PATH = 'rookeries/static/scripts'
    print('Removing compiled JS files...')
    inv.run('rm -f {source}/*.js'.format(source=SCRIPTS_PATH), echo=True)

    print('Remove compiled CSS files...')
    inv.run('rm -f {source}/*.css'.format(source=LESS_PATH), echo=True)

    print('Remove webassets...')
    inv.run('rm -f rookeries/static/.webassets-cache/*', echo=True)


@inv.task(docs.clean, test.clean, assets, python, name='all')
def clean_all():
    """Cleans up all generated files."""
    print('All clean!')
