"""
Unit tests for testing serving of page content.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import httplib
import json
import mock
import pathlib

import pytest

import rookeries


@pytest.fixture
def flask_test_app():
    return rookeries.make_rookeries_app().test_client()


@pytest.fixture
def expected_json_content():
    with pathlib.Path('docs/about.md').open('r', encoding='UTF-8') as markdown_data:
        content = markdown_data.read()
    return content


def test_serve_journal_entry_view_returns_journal_entry_as_json(flask_test_app, expected_json_content):
    expected_info = {
        'page': 'about',
        'title': 'About Rookeries',
        'page_content': expected_json_content,
        'revision': mock.ANY,
    }

    actual = flask_test_app.get('/app/pages/about')

    assert actual.status_code == httplib.OK
    assert json.loads(actual.data) == expected_info


def test_serve_journal_entry_view_serving_landing_doc_returns_about_page(flask_test_app, expected_json_content):
    expected_info = {
        'page': 'about',
        'title': 'About Rookeries',
        'page_content': expected_json_content,
        'revision': mock.ANY,
    }

    actual = flask_test_app.get('/app/pages/landing')

    assert actual.status_code == httplib.OK
    assert json.loads(actual.data) == expected_info


def test_serve_journal_entry_view_returns_404_when_no_content_found(flask_test_app):
    expect_json = {
        'error': {
            'code': 404,
            'message': 'Not Found'
        }
    }

    actual = flask_test_app.get('/app/pages/missing')

    assert actual.status_code == httplib.NOT_FOUND
    assert json.loads(actual.data) == expect_json
