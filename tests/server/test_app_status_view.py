"""
Unit tests for the app status view.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import httplib
import json

import pytest

import rookeries


@pytest.fixture
def flask_test_app():
    return rookeries.make_rookeries_app().test_client()


def test_app_status_view_returns_app_info_as_a_json(flask_test_app):
    expected_info = {
        'app': 'rookeries',
        'version': rookeries.__version__,
    }

    actual = flask_test_app.get('/status')

    assert actual.status_code == httplib.OK
    actual_json = json.loads(actual.data)
    assert actual_json == expected_info
