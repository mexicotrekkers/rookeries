Feature: User Login
  A user login that allows user to login and logout of the system


Scenario: Allowed User can Login into Site
  Given I am the admin user with the correct credentials
  When I navigate to the index page
  And I am not logged in
  And I click on the login button
  And I enter in my credentials
  And I click login
  Then I can see I am logged in
