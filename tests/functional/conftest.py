"""
Extra configurations to allow for parameterized tests.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import pytest


def pytest_addoption(parser):
    parser.addoption("--server-hostname", action="store", default="localhost",
                     help="hostname of server under test: localhost, 127.0.0.1, myremote-server.net...")
    parser.addoption("--server-port", action="store", default="5000", help="port of server under test: 5000")


@pytest.fixture
def server_hostname(request):
    return request.config.getoption("--server-hostname")


@pytest.fixture
def server_port(request):
    return request.config.getoption("--server-port")
