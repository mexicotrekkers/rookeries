"""
Sample database test fixtures for database related tests.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import pathlib


def create_sample_page(page_id, title, content_file):
    sample_content_path = pathlib.Path('docs', content_file)
    with sample_content_path.open(encoding='utf-8') as markdown_file:
        content = markdown_file.read()

    return {'_id': page_id, 'title': title, 'content': content}


def bulk_docs_export():
    return [
        create_sample_page('about', title='About Rookeries', content_file='about.md'),
        create_sample_page('license', title='License - AGPL version 3', content_file='license.md'),
    ]
