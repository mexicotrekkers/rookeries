"""
Unit tests for base deployment base functionality.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import arrow
import mock

import rookeries
from tasks import deploy


def test_generate_package_name_returns_deployable_app_package_name_with_version():
    expected_filename = 'rookeries-{0}'.format(rookeries.__version__)
    actual_filename = deploy.generate_package_name()
    assert expected_filename == actual_filename


def test_generate_package_name_returns_deployable_app_package_name_with_suffix_when_suffix_included():
    test_suffix = 'TESTING'
    expected_filename = 'rookeries-{0}-{1}'.format(rookeries.__version__, test_suffix)
    actual_filename = deploy.generate_package_name(pkg_suffix=test_suffix)
    assert expected_filename == actual_filename


@mock.patch('arrow.now', autospec=True)
def test_generate_package_name_returns_deployable_app_package_name_with_date_when_date_suffix_flag_added(
        mock_arrow_now):
    expected_suffix = '19991231-1200'
    test_now = arrow.get(expected_suffix, 'YYYYMMDD-HHmm')
    mock_arrow_now.return_value = test_now
    expected_filename = 'rookeries-{0}-{1}'.format(rookeries.__version__, expected_suffix)

    actual_filename = deploy.generate_package_name(add_date_suffix=True)
    assert expected_filename == actual_filename
