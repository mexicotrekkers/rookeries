"use strict"

React = require "react"
TestUtils = require "react/lib/ReactTestUtils"
sinon = require "sinon"

Actions = require "../../client/actions.coffee"
JournalEntryStore = require "../../client/stores/journal_entry_store.coffee"
JournalMarkdownViewer = require "../../client/views/journal_markdown_viewer.cjsx"
Router = require "react-router"


describe 'JournalMarkdownViewer', () ->
    RealActions = null
    RealJournalEntryStore = null
    RealStateMixin = null
    StubStateMixin = sinon.stub(Router.State)

    describe "simple test", () ->
      it "prove that test infrastructure works", () ->
        assert.ok true

    describe.skip "Skip for now until component refactored", () ->

      beforeEach () ->
          console.log('Before')
          RealActions = JournalMarkdownViewer.__get__ "Actions"
          RealJournalEntryStore = JournalMarkdownViewer.__get__ "JournalEntryStore"
          RealStateMixin = JournalMarkdownViewer.__get__ "Router.State"
          JournalMarkdownViewer.__set__ "Actions", sinon.stub(Actions)
          JournalMarkdownViewer.__set__ "JournalEntryStore", sinon.stub(JournalEntryStore)
          JournalMarkdownViewer.__set__ "Router.State", StubStateMixin


      afterEach () ->
          console.log('After')
          JournalMarkdownViewer.__set__ "Actions", RealActions
          JournalMarkdownViewer.__set__ "JournalEntryStore", RealJournalEntryStore
          JournalMarkdownViewer.__set__ "Router.State", RealStateMixin


      it 'The view should default to a loading text when initializing and then calling for data', () ->
          @skip "Implement me later"

          # TODO Attempt to wrap the rendering of the route... using sinon stubs and knowledge of React's Router.
          # StubStateMixin.getParams.id = undefined
          mockStateCurrentParams = {getCurrentParams: () -> {id: undefined}}
          mockStateMixinContext = {context: {router: sinon.stub(mockStateCurrentParams, "getCurrentParams")}}
          StubStateMixin = sinon.stub(mockStateMixinContext)

          journalMarkdownView = TestUtils.renderIntoDocument(<JournalMarkdownViewer />)

          assert.ok journalMarkdownView

          journalTitle = TestUtils.findRenderedDOMComponentWithTag(journalMarkdownView, 'h1')
          assert.ok journalTitle
          assert.equal journalTitle.getDOMNode().textContent, 'Loading...'

          journalContent = TestUtils.findRenderedDOMComponentWithTag(journalMarkdownView, 'span')
          assert.ok journalContent
          assert.include journalContent.getDOMNode().textContent, 'Please wait while we load'

      # TODO Add timer test
      # TODO Add mock call for data.

      it 'The view should update its state given newer data', () ->
          @skip('Implement me.')
