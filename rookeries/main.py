"""
Package for running Rookeries.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""


import importlib

import flask

rookeries_app = flask.Blueprint('rookeries_app', __name__)


def register_views():
    """Import the various views in the app."""
    rookeries_views = [
        'rookeries.views',
        'rookeries.journal.views',
        'rookeries.security',
    ]
    for views_modules in rookeries_views:
        importlib.import_module(views_modules)

register_views()
