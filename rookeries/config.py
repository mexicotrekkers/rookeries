"""
Configuration for Rookeries app

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

from werkzeug import security

from flask_appconfig import env

ROOKERIES_ENV_CONFIG_PREFIX = 'ROOKERIES_'


class DefaultConfig(object):
    """Default configuration for Rookeries."""
    DEBUG = True
    COUCHDB_DATABASE_URI = 'http://admin:password@localhost:5984/'
    COUCHDB_DATABASE = 'rookeries'

    # Admin user credentials as Rookeries is a single user app.
    ADMIN_USERNAME = 'admin'
    ADMIN_PASSWORD = security.generate_password_hash('password')

    # Security configuration - Make sure you change this!!! - Used by Flask for session secrets.
    SECRET_KEY = 'secret-key'
    # This is required by Flask-JWT.
    JWT_SECRET_KEY = 'problematic_penguins'


def configure_web_app(web_app):
    """Configure the app first by its default than by a known filename and than an environment variable."""
    web_app.config.from_object(DefaultConfig)
    env.from_envvars(conf=web_app.config, prefix=ROOKERIES_ENV_CONFIG_PREFIX, as_json=False)
