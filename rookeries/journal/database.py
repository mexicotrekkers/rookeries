"""
Database code for handling the retrieval and storage of journal entries.

:copyright: Copyright 2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import copy

from rookeries import database
from rookeries.journal import models


class JournalEntryDataStore(database.DataStore):
    def map_to_domain(self, document):
        return models.JournalEntry(
            document['_id'], content=document['content'], title=document['title'], revision=document['_rev']
        )

    def map_to_document(self, document_id, domain):
        # TODO Clean up please.
        document_data = copy.deepcopy(domain)
        document_data['_id'] = document_id
        document_data['_rev'] = domain.get('revision')
        for key in ['id', 'revision']:
            del document_data[key]
        return document_data
