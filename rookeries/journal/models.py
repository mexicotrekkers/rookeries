"""
Models for the journal or CMS component.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""


class JournalEntry(object):
    def __init__(self, entry_id, title='', content='', revision=''):
        # TODO DAP - Add JSON mapping with jq.  Include a decorator for turning models into json.
        self.page = entry_id
        self.title = title
        self.page_content = content
        self.revision = revision
