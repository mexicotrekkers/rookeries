"""
Main package for Rookeries.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

from flask.ext import assets

from rookeries import config, main, webapp

__author__ = 'Dorian Pula'
__version__ = '0.5.0-alpha'


def make_rookeries_app():
    """Creates and setups a Rookeries webapp."""
    web_app = webapp.make_json_app(__name__)
    web_app.register_blueprint(main.rookeries_app)
    config.configure_web_app(web_app)
    register_asset_bundles_with_app(web_app)
    return web_app


def register_asset_bundles_with_app(web_app):
    asset_env = assets.Environment(web_app)

    bundled_css = assets.Bundle(
        'css/*.less',
        filters='less',
        output='css/rookeries-bundle.css'
    )

    vendor_css = assets.Bundle(
        assets.Bundle(
            '../../node_modules/font-awesome/css/font-awesome.css',
            output='css/font-awesome.css'
        ),
        assets.Bundle(
            '../../node_modules/bootstrap/dist/css/bootstrap-theme.css',
            output='css/bootstrap-theme.css'
        ),
        assets.Bundle(
            '../../node_modules/bootstrap/dist/css/bootstrap.css',
            output='css/bootstrap.css'
        ),
    )

    bundled_js = assets.Bundle(
        '../../client/dist/rookeries.js',
        output='scripts/rookeries-bundle.js',
    )

    asset_env.register('bundled_css', bundled_css)
    asset_env.register('bundled_js', bundled_js)
    asset_env.register('vendor_css', vendor_css)
