# OpenFonts

The following fonts from the [OpenFonts library](http://openfontlibrary.org/) are packaged as part of Rookeries, to 
speed up development.

All fonts are licensed as per the license documents in their respective directories.

The CSS files were grabbed from http://openfontlibrary.org/faces/$FONTFACE and the interal URI modified appropriately.
