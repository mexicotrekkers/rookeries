# coding=utf-8
"""
Core web application views.

:copyright: Copyright 2013-2015, Dorian Pula <dorian.pula@amber-penguin-software.ca>
:license: AGPL v3+
"""

import flask

import rookeries
from rookeries.main import rookeries_app


@rookeries_app.route('/status')
def app_status():
    """Simple view that returns the status of the application.  Useful as a part of a heartbeat mechanism."""
    app_status_info = {
        'app': 'rookeries',
        'version': rookeries.__version__,
    }

    return flask.jsonify(app_status_info)


@rookeries_app.route('/app/config')
def app_config():
    # TODO Make testable and configurable
    webapp_config = {
        'siteName': 'Rookeries',
        'siteTagline': 'Simple Site Scaffolding!',
        'siteLogo': '/static/images/mr-penguin-amber.svg',
        'siteFooter': u'Rookeries - © 2013-2015 - Amber Penguin Software',
        'siteFavicon': '/static/mr-penguin-amber-favicon.ico',
    }
    return flask.jsonify(webapp_config)


@rookeries_app.route('/app/menu')
def menu_config():
    # TODO Make testable and configurable
    nav_menu = {'menu': generate_menu()}
    return flask.jsonify(nav_menu)


@rookeries_app.route("/")
@rookeries_app.route("/<path:whatever>")
def serve_document(whatever=None):
    return flask.render_template("base.html")


def generate_menu():
    # TODO Make this generated from the "activated" modules.
    # TODO Add in user login and registration.
    return [
        {'name': 'About', 'url': '/about'},
        {'name': 'License', 'url': '/license'},
        {'name': 'Experimental', 'url': '/about'}
    ]
