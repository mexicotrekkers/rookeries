###
Store backing the theme switcher view.

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

Reflux = require "reflux"
request = require "superagent"

Actions = require "../actions.coffee"
UserInfo = require "../user_info.coffee"


ThemeSwitchStore = Reflux.createStore(
    getRegisteredThemes: () ->
        return ["daytime", "evening"]

    init: () ->
        userTheme = UserInfo.getUserInfo("theme")
        @setTheme(userTheme)
        @listenTo(Actions.switchTheme, @switchTheme)

    setTheme: (desiredTheme) ->
        registeredThemes = @getRegisteredThemes()
        if desiredTheme not in registeredThemes
            console.error "Selected theme '#{desiredTheme}' is not one of the known themes: '#{registeredThemes}'"
            desiredTheme = registeredThemes[0]

        currentTheme = @getCurrentTheme()
        if currentTheme != desiredTheme
            console.log "Adding '#{desiredTheme}' and removing '#{currentTheme}'"
            document.body.classList.remove(currentTheme)
            document.body.classList.add(desiredTheme)
        
        UserInfo.setUserInfo("theme", desiredTheme)
        Actions.refreshTheme(@getNextTheme())

    getCurrentTheme: () ->
        currentThemes = document.body.classList
        return currentThemes[0]

    getNextTheme: () ->
        return if @getCurrentTheme() == "daytime" then "evening" else "daytime"

    switchTheme: (desiredTheme) ->
        currentTheme = @getCurrentTheme()      
        console.log "Switch themes from '#{currentTheme}' to '#{desiredTheme}'!"
        @setTheme(desiredTheme)

)

module.exports = ThemeSwitchStore
