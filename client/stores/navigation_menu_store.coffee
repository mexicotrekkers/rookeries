###
Store handling navigation menu.

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL-1.0
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

Reflux = require "reflux"
request = require "superagent"

Actions = require "../actions.coffee"


NavigationMenuStore = Reflux.createStore(
    init: () ->
        @listenTo(Actions.fetchNavigationMenu, @fetchNavigationDetails)

    fetchNavigationDetails: () ->
        request.get("/app/menu")
        .accept('json')
        .end(
            (err, res) ->
                if (res.ok)
                    Actions.renderNavigationMenu({menu: res.body.menu})
                else
                    console.error("Error getting document JSON - #{res.status} - #{res.text} - Actual #{err}")
        )
)

module.exports = NavigationMenuStore
