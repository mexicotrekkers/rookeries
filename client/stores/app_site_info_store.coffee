###
Store handling app site information.

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL-1.0
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

Reflux = require "reflux"
request = require "superagent"

Actions = require "../actions.coffee"


AppSiteInfoStore = Reflux.createStore(
    init: () ->
        @listenTo(Actions.fetchAppSiteInfo, @fetchAppSiteInfo)

    fetchAppSiteInfo: () ->
        request.get("/app/config")
        .accept('json')
        .end(
            (err, res) ->
                if (res.ok)
                    Actions.renderAppSiteInfo(res.body)
                else
                    console.error("Error getting document JSON - #{res.status} - #{res.text} - Actual #{err}")
        )
)

module.exports = AppSiteInfoStore
