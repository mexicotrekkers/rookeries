###
User login store.

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

Reflux = require "reflux"
request = require "superagent"

Actions = require "../actions.coffee"
UserInfo = require "../user_info.coffee"


UserLoginStore = Reflux.createStore(
    init: () ->
        @listenTo(Actions.loginUserAttempt, @attemptUserLogin)
        @listenTo(Actions.logoutUser, @logoutUser)

    isUserLoggedIn: () ->
    	return UserInfo.getUserInfo("auth_token") != ""

    getUserAuthToken: () ->
        return UserInfo.getUserInfo("auth_token")

    getCurrentUserFullName: () ->
        return UserInfo.getUserInfo("full_name")

    attemptUserLogin: (username, password) ->
        request.post("/auth")
        .send({username: username, password: password})
        .accept('json')
        .end(
            (err, res) ->
                if (res.ok)
                    # TODO Include the user full name in response as well.
                    UserLoginStore.updateUserLogin(res.body.token, "admin")
                    Actions.loginUserSuccess()
                else
                    console.error("Unable to login - #{res.status} - #{res.text} - Actual #{err}")
                    Actions.loginUserFailure("Login failed.  Try again!")
        )

    logoutUser: () ->
        UserInfo.removeUserInfo("auth_token")
        UserInfo.removeUserInfo("full_name")
        Actions.logoutUserFinalize()


    updateUserLogin: (token, fullName) ->
        if token? and token != ""
            UserInfo.setUserInfo("auth_token", token)
            UserInfo.setUserInfo("full_name", fullName)
        else
            console.log "Invalid token received #{token}"
            Actions.loginUserFailure("Received invalid token: #{token}")

)

module.exports = UserLoginStore
