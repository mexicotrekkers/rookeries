###
Rookeries client - Journal entry store.

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

Reflux = require "reflux"
request = require "superagent"

Actions = require "../actions.coffee"
UserLoginStore = require "../stores/user_login_store.coffee"


JournalEntryStore = Reflux.createStore(
    init: () ->
        @listenTo(Actions.loadEntry, @loadEntry)
        @listenTo(Actions.saveEntry, @savePageToDatabase)

    loadEntry: (id) ->
        if id is undefined
            id = "landing"
        request.get("/app/pages/#{id}")
        .accept("json")
        .end(
            (err, res) ->
                if (res.ok)
                    Actions.viewEntry({title: res.body.name, pageContent: res.body.page_content, id: res.body.page})
                else
                    console.error("Error getting document JSON - #{res.status} - #{res.text} - Actual #{err}")
                    Actions.showError({status: res.status, message: res.text, actual: err, title: res.body.error.message})
        )

    canEditPage: () ->
        # TODO Add in a way to make sure that this particular page
        return UserLoginStore.isUserLoggedIn()

    savePageToDatabase: (page) ->
        if not UserLoginStore.isUserLoggedIn()
            console.log "Login before saving content."
            return

        authToken = UserLoginStore.getUserAuthToken()

        request.put("/app/pages/#{page.id}")
        .send(page)
        .set("Authorization", "Bearer #{authToken}")
        .accept("json")
        .end(
            (err, res) ->
                if (res.ok)
                    Actions.viewEntry({title: res.body.name, pageContent: res.body.page_content})
                else
                    console.error("Error saving document JSON - #{res.status} - #{res.text} - Actual #{err}")
                    Actions.showError({status: res.status, message: res.text, actual: err, title: res.body.error.message})
        )

)

module.exports = JournalEntryStore
