###
Rookeries client app - Route setup

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

'use strict'

React = require('react')
Router = require('react-router')

JournalMarkdownView = require('./views/journal_markdown_viewer.cjsx')

AppRoutes = (
    <Router.Route handler={JournalMarkdownView} path="/">
        <Router.Route name='pageFetch' path='/:id' handler={JournalMarkdownView} />
    </Router.Route>
)

module.exports = AppRoutes
