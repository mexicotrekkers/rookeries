###
Navigation Menu

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL-1.0
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

React = require "react"
Reflux = require "reflux"

Actions = require "../actions.coffee"
NavigationMenuStore = require "../stores/navigation_menu_store.coffee"


NavigationMenu = React.createClass(
    mixins: [Reflux.ListenerMixin]

    getInitialState: () ->
        return {
            "menu": [{"name": "Home", "url": "/"}]
        }

    updateMenu: (menu) ->
        @setState(menu)

    componentDidMount: () ->
        @listenTo(Actions.renderNavigationMenu, @updateMenu)
        Actions.fetchNavigationMenu()   

    render: () ->
        return (
            <div className="col-lg-2 col-lg-offset-1 ice-menu">
                <ul className="nav nav-stacked nav-pills">
                    {@state.menu.map((menuItem) -> <li><a href={menuItem.url}>{menuItem.name}</a></li>)}
                </ul>
            </div>
        )
)

module.exports = NavigationMenu
