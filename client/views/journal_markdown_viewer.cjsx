###
Markdown controller to view and update journal entries.

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

React = require "react"
Reflux = require "reflux"
marked = require "marked"
Router = require "react-router"
Bootstrap = require "react-bootstrap"
FontAwesome = require "react-fontawesome"

Actions = require "../actions.coffee"
JournalEntryStore = require "../stores/journal_entry_store.coffee"
UserLoginStore = require "../stores/user_login_store.coffee"

CodeMirror = require "react-codemirror"

# TODO Remove and rework to allow for two flags, rather than a single state?
EditorPaneState = {
    hidden: 0,
    editableClosed: 1,
    editableOpen: 2
}


JournalMarkdownView = React.createClass(
    mixins: [Reflux.ListenerMixin, Router.State]

    getInitialState: () ->
        console.log "Initialize JournalMarkdownView state."
        return {
            id: "",
            name: "Loading...", 
            displayContent: "Please wait while we load this journal entry.",
            originalContent: "Please wait while we load this journal entry.",
            editorPaneState: EditorPaneState.hidden,
            editorTheme: "monokai"
        }

    onStatusChange: (data) ->
        console.log "Update status of JournalMarkdownView."
        @setState({
            id: data.id,
            name: data.title, 
            displayContent: data.pageContent,
            originalContent: data.pageContent
        })
        @updateEditAllowance()

    updateEditAllowance: () ->
        allowEdits = JournalEntryStore.canEditPage()
        editorState = @state.editorPaneState

        if not allowEdits
            editorState = EditorPaneState.hidden
        
        if allowEdits and @state.editorPaneState == EditorPaneState.hidden
            editorState = EditorPaneState.editableClosed
        @setState({editorPaneState: editorState})

    onErrorStatus: (error) ->
        console.log "Update status of JournalMarkdownView with error."
        # TODO Translate this out better.
        @setState({name: "#{error.title} - #{error.status}", displayContent: "JSON - #{error.message}"})

    updateCode: (newCode) ->
        @setState({displayContent: newCode})

    componentDidMount: () ->
        console.log "Mount JournalMarkdownView."
        @listenTo(Actions.viewEntry, @onStatusChange)
        @listenTo(Actions.loginUserSuccess, @updateEditAllowance)
        @listenTo(Actions.loginUserFailure, @updateEditAllowance)
        @listenTo(Actions.logoutUserFinalize, @updateEditAllowance)
        @listenTo(Actions.showError, @onErrorStatus)

        pageToLoad = @getParams().id
        console.log "Trigger JournalEntryAction.loadEntry event - #{pageToLoad}."
        Actions.loadEntry(pageToLoad)

    statics: {
        willTransitionTo: (transition, params, query, callback) ->
            console.log "Transition for JournalMarkdownView triggered."
            pageToLoad = params.id
            console.log "Trigger JournalEntryAction.loadEntry event - #{pageToLoad}."
            Actions.loadEntry(pageToLoad)
            callback()
    }

    toggleEditorPane: () ->
        nextEditorState = @state.editorPaneState
        if @state.editorPaneState == EditorPaneState.editableOpen
            nextEditorState = EditorPaneState.editableClosed

        if @state.editorPaneState == EditorPaneState.editableClosed
            nextEditorState = EditorPaneState.editableOpen

        @setState({editorPaneState: nextEditorState})

    isEditorPaneOpen: () ->
        return @state.editorPaneState == EditorPaneState.editableOpen

    resetContent: () ->
        @setState({displayContent: @state.originalContent})

    saveContent: () ->
        # TODO Start using proper models for page and json
        pageContent = {
            id: @state.id,
            title: @state.name,
            content: @state.displayContent
        }
        Actions.saveEntry(pageContent)

    render: () ->
        console.log "Render JournalMarkdownView."
        code = @state.displayContent
        rawMarkup = marked(code, {sanitized: true})

        # TODO Option to switch between monokai and default CodeMirror themes.
        options = {mode:"markdown", theme: @state.editorTheme}

        # TODO Factor the view into something nicer.
        showEditorButton = (
            <Bootstrap.Button onClick={ @toggleEditorPane }>
                <FontAwesome name="edit" /> Edit
            </Bootstrap.Button>
        )
        # TODO Add in ability to display messages outside the collapsible pane
        showPanel = (
            <Bootstrap.Panel collapsible expanded={ @isEditorPaneOpen() }>
                <CodeMirror value={ code } onChange={ @updateCode } options={ options } />
                <Bootstrap.Button onClick={ @saveContent }><FontAwesome name="save" /> Save </Bootstrap.Button>
                <Bootstrap.Button onClick={ @resetContent }><FontAwesome name="undo" /> Reset </Bootstrap.Button>
            </Bootstrap.Panel>
        )
        if @state.editorPaneState == EditorPaneState.hidden
            showEditorButton = (<div></div>)
            showPanel = (<div></div>)

        return (
            <div>
                <h1>{ @state.name }</h1>
                <span dangerouslySetInnerHTML={{__html: rawMarkup}} />
                {showEditorButton}
                {showPanel}
            </div>
        )
)

module.exports = JournalMarkdownView
