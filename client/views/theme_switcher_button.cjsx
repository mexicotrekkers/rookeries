###
Switches between premade themes for the site.

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL-1.0
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

React = require "react"
Reflux = require "reflux"
Bootstrap = require "react-bootstrap"
FontAwesome = require "react-fontawesome"

Actions = require "../actions.coffee"
ThemeSwitchStore = require "../stores/theme_switcher_store.coffee"


ThemeSwitchView = React.createClass(
    mixins: [Reflux.ListenerMixin]

    getInitialState: () ->
        return {alternativeTheme: ThemeSwitchStore.getNextTheme()}

    switchTheme: () ->
        Actions.switchTheme(@state.alternativeTheme)

    updateAlternativeTheme: () ->
        @setState({alternativeTheme: ThemeSwitchStore.getNextTheme()})

    componentDidMount: () ->
        @listenTo(Actions.refreshTheme, @updateAlternativeTheme)

    render: () ->
        return (
            <div className="text-center muted hoverable" onClick=@switchTheme>
                <FontAwesome name="eye" />
                &nbsp; Change to {@state.alternativeTheme} theme
            </div>
        )
)

module.exports = ThemeSwitchView
