###
Site header

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL-1.0
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

React = require "react"
Reflux = require "reflux"

Actions = require "../actions.coffee"
AppSiteInfoStore = require "../stores/app_site_info_store.coffee"


SiteHeader = React.createClass(
    mixins: [Reflux.ListenerMixin]

    getInitialState: () ->
        return {siteName: "Rookeries", siteLogo: "/static/images/mr-penguin-amber.svg", siteTagline: "Simple Site Scaffolding"}

    updateHeaderInfo: (app) ->
        @setState(app)

    componentDidMount: () ->
        @listenTo(Actions.renderAppSiteInfo, @updateHeaderInfo)
        Actions.fetchAppSiteInfo()

    render: () ->
        return (
            <div className="row">
                <div className="col-lg-10 col-lg-offset-1">
                    <div className="row sea-header">
                        <div className="col-lg-2 col-lg-offset-3">
                            <img className="header_logo" src={@state.siteLogo} alt="#{@state.siteName} logo"/>
                        </div>
                        <div class="col-lg-4">
                            <h1 className="header_title">{@state.siteName}</h1>

                            <h2 className="header_tagline">{@state.siteTagline}</h2>
                        </div>
                    </div>
                </div>
            </div>
        )
)

module.exports = SiteHeader
