###
Site footer

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL-1.0
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

React = require "react"
Reflux = require "reflux"

Actions = require "../actions.coffee"
AppSiteInfoStore = require "../stores/app_site_info_store.coffee"


SiteFooter = React.createClass(
    mixins: [Reflux.ListenerMixin]

    getInitialState: () ->
        return {siteFooter: "Rookeries - © 2013-2015 - Amber Penguin Software"}

    updateFooterInfo: (app) ->
        @setState(app)

    componentDidMount: () ->
        @listenTo(Actions.renderAppSiteInfo, @updateFooterInfo)

    render: () ->
        return (
            <div className="row">
                <div className="col-lg-6 col-lg-offset-3">
                    <footer>{@state.siteFooter}</footer>
                </div>
            </div>
        )
)

module.exports = SiteFooter
