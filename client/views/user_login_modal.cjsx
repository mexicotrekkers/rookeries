###
User login and logout controller

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

React = require "react/addons"
Bootstrap = require "react-bootstrap"
FontAwesome = require "react-fontawesome"
Reflux = require "reflux"

Actions = require "../actions.coffee"
UserLoginStore = require "../stores/user_login_store.coffee"


UserLoginView = React.createClass(
    mixins: [React.addons.LinkedStateMixin, Reflux.ListenerMixin]

    getInitialState: () ->
        return {
            isUserLoggedIn: UserLoginStore.isUserLoggedIn(),
            fullName: UserLoginStore.getCurrentUserFullName(),
            username: "", 
            password: "",
            errorMessage: "",
            displayLoginModal: false
        }

    loginAttempt: () ->
        if @state.username == "" or @state.password == ""
            @loginFailure("Enter a username and password to login in...")
            return

        console.log "Attempting login..."
        Actions.loginUserAttempt(@state.username, @state.password)

    loginFailure: (errorMessage = "") ->
        @setState({errorMessage: errorMessage, isUserLoggedIn: false})

    loginSuccess: () ->
        @hideLoginModal()
        @setState({fullName: UserLoginStore.getCurrentUserFullName(), isUserLoggedIn: true})

    componentDidMount: () ->
        @listenTo(Actions.loginUserSuccess, @loginSuccess)
        @listenTo(Actions.loginUserFailure, @loginFailure)
        @listenTo(Actions.logoutUserFinalize, @logoutUserRefresh)

    showLoginModal: () ->
        @setState({displayLoginModal: true})

    hideLoginModal: () ->
        @setState({username: "", password: "", errorMessage: "", displayLoginModal: false})
        
    logoutUser: () ->
        console.log "Logging out user..."
        Actions.logoutUser()

    logoutUserRefresh: () ->
        @setState({errorMessage: "", isUserLoggedIn: false, fullName: UserLoginStore.getCurrentUserFullName()})

    render: () ->
        loggedIn = @state.isUserLoggedIn
        loginMessage = if loggedIn then "logout" else "login"
        loginIcon = if loggedIn then <FontAwesome name="sign-in" /> else <FontAwesome name="sign-out" />
        loginOperation = if loggedIn then @logoutUser else @showLoginModal
        errorDisplay = <Bootstrap.Alert bsStyle="danger">{ @state.errorMessage }</Bootstrap.Alert> if @state.errorMessage? and @state.errorMessage != ""

        return(
            <div>
                <div className="text-center muted hoverable" onClick={loginOperation}>
                    <FontAwesome name="user" />
                    &nbsp;
                    Hello {@state.fullName}!
                    &ensp;
                    {loginIcon}
                    &nbsp;
                    Do you want to {loginMessage}?
                </div>
                <Bootstrap.Modal show={@state.displayLoginModal} onHide={@hideLoginModal}>
                    <Bootstrap.Modal.Header closeButton>
                        <Bootstrap.Modal.Title>User Login</Bootstrap.Modal.Title>
                    </Bootstrap.Modal.Header>

                    <Bootstrap.Modal.Body>
                        {errorDisplay}
                        <form className="form-horizontal">
                            <Bootstrap.Input type="text" id="username" placeholder="Username" label="Username" valueLink={@linkState("username")}
                                labelClassName="col-xs-2" wrapperClassName="col-xs-10" />
                            <Bootstrap.Input type="text" id="password" placeholder="***" label="Password" valueLink={@linkState("password")}
                                labelClassName="col-xs-2" wrapperClassName="col-xs-10" />
                        </form>
                    </Bootstrap.Modal.Body>

                    <Bootstrap.Modal.Footer>
                        <Bootstrap.Button bsStyle="warning" onClick={@hideLoginModal}>Cancel</Bootstrap.Button>
                        <Bootstrap.Button onClick={@loginAttempt}>Login</Bootstrap.Button>
                    </Bootstrap.Modal.Footer>
                </Bootstrap.Modal>
            </div>
        )
)


module.exports = UserLoginView
