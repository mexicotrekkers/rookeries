###
User preferences via local storage

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###


UserInfo = {
	initialInfo: {
		theme: "evening",
		editor_theme: "monokai",
		full_name: "Stranger",
		auth_token: ""
	}

	getUserInfo: (info) ->
		console.log "Getting user info '#{info}'"
		storageKey = "user_#{info}"
		infoItem = localStorage.getItem(storageKey)
		if not infoItem?
			infoItem = @initialInfo[info]

		return infoItem

	setUserInfo: (info, value) ->
		console.log "Setting user info '#{info}' to '#{value}'"
		localStorage.setItem("user_#{info}", value)

	removeUserInfo: (info) ->
		console.log "Clearing user info '#{info}'"
		localStorage.removeItem("user_#{info}")

}

module.exports = UserInfo
