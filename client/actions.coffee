###
Action events used across the app.

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

Reflux = require "reflux"

Actions = Reflux.createActions [
    "viewEntry"
    "loadEntry"
    "saveEntry"
    "showError"
    "switchTheme"
    "refreshTheme"
    "fetchNavigationMenu"
    "renderNavigationMenu"
    "fetchAppSiteInfo"
    "renderAppSiteInfo"
    "loginUserAttempt"
    "loginUserSuccess"
    "loginUserFailure"
    "logoutUser"
    "logoutUserFinalize"
]

module.exports = Actions
