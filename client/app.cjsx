###
Rookeries client app

@copyright (c) Copyright 2013-2015 Dorian Pula
@license AGPL v3
@author Dorian Pula [dorian.pula@amber-penguin-software.ca]
###

"use strict"

React = require "react"
Router = require "react-router"

AppRoutes = require "./routes.cjsx"
UserLoginView = require "./views/user_login_modal.cjsx"
ThemeSwitchView = require "./views/theme_switcher_button.cjsx"

SiteHeader = require "./views/site_header.cjsx"
SiteFooter = require "./views/site_footer.cjsx"
NavigationMenu = require "./views/navigation_menu.cjsx"

Router.run(AppRoutes, Router.HistoryLocation, (Root) =>
    React.render(<Root />, document.getElementById("markdown-target"))
)

React.render(<ThemeSwitchView />, document.getElementById("theme-switcher"))
React.render(<UserLoginView />, document.getElementById("user-login-trigger"))
React.render(<SiteHeader />, document.getElementById("site-header"))
React.render(<SiteFooter />, document.getElementById("site-footer"))
React.render(<NavigationMenu />, document.getElementById("navigation-menu"))
