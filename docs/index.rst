
Rookeries
=========

A blogging and microsite CMS for building modern, mobile-ready, flexible and maintainable websites.

**Rookeries** is:

- Powered by Python on the server side using Flask and CouchDB.
- Uses React, Reflux, CoffeeScript and LESS to build responsive single page apps.
- Licensed under the Affero GNU Genearal Public License (AGPL) version 3.0.

**Status**

- Builds: |build_status|

.. |build_status| image:: https://codeship.com/projects/925314d0-d22c-0132-991b-16c1124d299d/status?branch=master
:target: https://codeship.com/projects/77461


Contents:

.. toctree::
   :maxdepth: 2

   about
   changelog
   license

Code Directory
--------------

.. toctree::
   :maxdepth: 3

   _api/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

