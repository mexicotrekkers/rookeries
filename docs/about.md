# Rookeries

A developer and designer friendly web platform for building gorgeous blogs and portfolios.

**Rookeries** is:

- Powered by Python on the server side using Flask and CouchDB.
- Uses React, Reflux, CoffeeScript and LESS to build responsive single page apps.
- Licensed under the Affero GNU Genearal Public License (AGPL) version 3.0.
- Heavily inspired by the [Ghost blogging platform](https://ghost.org/about/).
