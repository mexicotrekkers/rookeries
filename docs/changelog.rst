=========
Changelog
=========

* :feature:`6` Add CouchDB persistance of content.
* :support:`0` Simplify layout of Rookeries into something simpler to showcase the new focus of the application.
* :support:`0` Major rework for application and code clean-up.
* :support:`0` Transition to using React as frontend tech.
* :support:`0` Addition of proper testing infrastructure.

* :release:`0.4.9 <2015-07-13>`
* :feature:`0` Separate frontend and deployment scripts into standalone projects.  See `rookeries-react-app
  <https://bitbucket.org/dorianpula/rookeries-react-app>`_ and `ansible-rookeries
  <https://bitbucket.org/dorianpula/ansible-rookeries>`_.
* :support:`0` Simplify and update Python requirements.
* :support:`0` Make codebase :pep:`8` compliant.
* :support:`0` Add tasks for testing with nose and coverage.
* :support:`0` Add Sphinx documentation.
* :bug:`0` Migrate administrative functionality into separate invoke tasks.

* :release:`0.3.0 <2013-07-25>`
* :support:`0` Started using enums for some constrainted model values using
  `enum34 <https://pypi.python.org/pypi/enum34>`_ and
  `SQLAlchemy's Enums <http://techspot.zzzeek.org/2011/01/14/the-enum-recipe/>`_. [2013 June 28]
* :feature:`0` Built out user account management and registration. [2013 June 30]
* :support:`0` Added `special JSON string responses for Flask  <http://flask.pocoo.org/snippets/83/>`_ [2013 July 03]
* :feature:`0` Added switchable CSS themes. [2013 July 04]
* :support:`0` Transitioned to `LESS <http://lesscss.org/>`_ to simplify CSS with mixins, using colour hues,
  et cetera. [2013 July 08]
* :support:`0` Start task and project sub-project.
* :feature:`0` Added in AngularJS frontend driven by JSON responses.

* :release:`0.2.0 <2013-06-25>`
* :support:`0` Transitioned documentation to reStructured text. [2013 May 09]
* :feature:`0` Migrated static reStructured text pages to Markdown.
* :feature:`0` Reorganized webapp into a modular structure.

* :release:`0.1.0 <2013-04-24>`
* :feature:`0` Initial release of rookeries.
