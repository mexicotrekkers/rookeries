# Rookeries

A developer and designer friendly web platform for building gorgeous blogs and portfolios.

**Rookeries** is:

- Powered by Python on the server side using Flask and CouchDB.
- Uses React, Reflux, CoffeeScript and LESS to build responsive single page apps.
- Licensed under the Affero GNU General Public License (AGPL) version 3.0.
- Heavily inspired by the [Ghost blogging platform](https://ghost.org/about/).

**Status**

- Builds: [ ![Codeship Status for dorianpula/rookeries](https://codeship.com/projects/925314d0-d22c-0132-991b-16c1124d299d/status?branch=master)](https://codeship.com/projects/77461)

## Installation

*Rookeries* is heavily under development and once things stabilize there will be a user friendly installation guide.
In the meantime, please refer to the development guide below.

## Development

*Rookeries* uses the following technologies:
 - Python
 - NodeJS
 - CoffeeScript
 - GCC (for compiling some dependencies)

Make sure you have the following installed and setup before continuing:
- git
- pip
- virtualenv
- npm

comfortable using the bash shell on Linux or Mac OS X.  (Window users should install bash using
[cygwin](http://cygwin.com/) or the Windows App store.)

For development and deployment you'll also need [Vagrant](https://www.vagrantup.com/) and
[Ansible](http://ansible.com/).

### Getting Started

1. Clone this repository: `git clone https://bitbucket.org/dorianpula/rookeries.git`
1. Create a virtualenv and source it: `virtualenv rookeries && source rookeries/bin/activate`
1. Install the Python/PIP dependencies: `pip install -r requirement/development.txt`
    - *Ubuntu Linux*: You may need to install via `apt-get install` the following dependencies:
        - python-dev
        - libffi-dev
        - libssl-dev
1. Install the Node/NPM dependencies: `npm install`
1. Install the global Node/NPM dependencies: `npm install -g coffee-script browserify less karma-cli`
1. Run the invoke build script to run the application: `inv server.run`
1. Navigate to the main Rookeries page in a browser: [http://localhost:5000/](http://localhost:5000/)

Use `inv -l` to list all the available commands.

### Test Deployment

You can also test the deployment of *Rookeries* to local server.  You'll need to install to have
[Vagrant](https://www.vagrantup.com/).  [Ansible](http://ansible.com/) should be installed in your virtualenv if
you followed the instructions above.

1. Install the Rookeries to a local Vagrant box with: `inv vagrant.install`
1. The application will be accessible via port 8080 locally: [http://localhost:8080/](http://localhost:8080/)
